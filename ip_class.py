class ip:
    def __init__(self):
        self.ip = ip
        
    def show(self, comment, address):
        print(comment+" : "+ip_class.zfill(address)+"\t"+ip_class.address_2_bin(address))

    def total_host(self, mask):
        host = 2**(32-mask)-2
        print("Hosts/Net : "+str(host))

    def int_2_bin(self, num):
        binary = "{0:032b}".format(num)
        octet = ['','','','']
        octet[0] = str(int(binary[0:8],2))
        octet[1] = str(int(binary[8:16],2))
        octet[2] = str(int(binary[16:24],2))
        octet[3] = str(int(binary[24:32],2))
        return octet[0]+"."+octet[1]+"."+octet[2]+"."+octet[3]

    def int_2_bin(self, num):
        binary = "{0:032b}".format(num)
        octet = ['','','','']
        octet[0] = str(int(binary[0:8],2))
        octet[1] = str(int(binary[8:16],2))
        octet[2] = str(int(binary[16:24],2))
        octet[3] = str(int(binary[24:32],2))
        return octet[0]+"."+octet[1]+"."+octet[2]+"."+octet[3]

    def address_2_bin(self, address):
        address = address.split(".")
        return '{0:08b}'.format(int(address[0]))+"."+'{0:08b}'.format(int(address[1]))+"."+'{0:08b}'.format(int(address[2]))+"."+'{0:08b}'.format(int(address[3]))

    def split_address(self, address,mask):
        address = address.split(".") 
        octet = ['','','','']
        octet[0] = int(address[0]) * 16777216
        octet[1] = int(address[1]) * 65536
        octet[2] = int(address[2]) * 256
        octet[3] = int(address[3])
        address_bin = '{0:32b}'.format(octet[0] + octet[1] + octet[2] + octet[3])
        return address_bin[0:mask]

    def zfill(self, address):
        address = address.split(".")
        return address[0].zfill(3)+"."+address[1].zfill(3)+"."+address[2].zfill(3)+"."+address[3].zfill(3)

ip_class = ip()
    
while(1==1):

    address = ""

    try:
        address = input("Please input your ip : ")
        address[address.index("/")+1:]
    except ValueError:
        address = input("Format error please follow format like this : 192.168.0.1/24")
        address = "192.168.0.1/24"

    All = False
    try:
        if (address.index("-a") > 0):
            All = True
            address = address[0:address.index("-a")]
    except ValueError:
        All = False

    ip = address.split("/")[0]
    mask = int(address.split("/")[1])

    print()
    ip_class.show("Address  ", ip)
    ip_class.show("Netmask  ", ip_class.int_2_bin(2**32-2**(32-mask)))
    ip_class.show("Wildcard ", ip_class.int_2_bin(2**(32-mask)-1))
    print()
    netword_id = ip_class.split_address(ip, mask)
    ip_class.show("Network  ", ip_class.int_2_bin(int(netword_id,2)*2**(32-mask)))
    ip_class.show("Broadcast", ip_class.int_2_bin(int(netword_id,2)*2**(32-mask)+2**(32-mask)-1))
    ip_class.show("HostMin  ", ip_class.int_2_bin(int(netword_id,2)*2**(32-mask)+1))
    ip_class.show("HostMax  ", ip_class.int_2_bin(int(netword_id,2)*2**(32-mask)+2**(32-mask)-1-1))
    if(All == True):
        for i in range (1,2**(32-mask)-1):
            ip_class.show("Host     ", ip_class.int_2_bin(int(netword_id,2)*2**(32-mask)+i))
    print()
    ip_class.total_host(mask)
    print()
    print()
